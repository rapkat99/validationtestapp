//
//  ValidatorTestAppTests.swift
//  ValidatorTestAppTests
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import XCTest
@testable import ValidatorTestApp

class ValidatorTestAppTests: XCTestCase {

    var signUpViewController: SignUpViewController!
    var passwordContainView: PasswordContainView!
    
    override func setUpWithError() throws {
        signUpViewController = SignUpViewController()
        passwordContainView = PasswordContainView()
    }

    override func tearDownWithError() throws {
        signUpViewController = nil
        passwordContainView = nil
        try super.tearDownWithError()
    }

    func testExample() throws {
        signUpViewController.viewModel.update(email: "test@gmail.com")
        signUpViewController.viewModel.checkEmailValidation()
        let passwordStatus = passwordContainView.viewModel.checkIfPasswordIsCorrect(password: "Test625test")
        
        XCTAssert(signUpViewController.viewModel.isEmailCorrect, "Email validation status")
        XCTAssert(passwordStatus, "Password validation status")
        
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
