//
//  PasswordContainViewViewModel.swift
//  ValidatorTestApp
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import Foundation

protocol PasswordContainViewViewModelProtocol {
    func didUpdateStatus(part: PasswordContainView.ContainPart)
}

class PasswordContainViewViewModel {
    var delegate: PasswordContainViewViewModelProtocol?
    
    func checkIfPasswordIsCorrect(password: String) -> Bool {
        var isValid = true
        
        if !characterCountIsCorrect(password: password) {
            isValid = false
        }
        
        if !lowerCaseIsCorrect(password: password) {
            isValid = false
        }
        
        if !upperCaseIsCorrect(password: password) {
            isValid = false
        }
        
        if !numberIsCorrect(password: password) {
            isValid = false
        }
        
        if !specialCharactersAndSpacesIsCorrect(password: password) {
            isValid = false
        }
        
        if !englishCharactersIsCorrect(password: password) {
            isValid = false
        }
        
        if !seriesOfCharactersIsMissing(password: password) {
            isValid = false
        }
        
        return isValid
    }
    
    private func seriesOfCharactersIsMissing(password: String) -> Bool {
        if password.isEmpty {
            delegate?.didUpdateStatus(part: .seriesOfCharacters(false))
            
            return false
        }
        
        let password = password.lowercased()
        let charactesArray = Array(password)
        
        var maxSeriesCount = 0
        var seriesCount = 1
        var lastChar: String.Element?
        
        charactesArray.forEach { character in
            if (lastChar != nil) {
                if ((lastChar!.hexDigitValue ?? 0) + 1 == character.hexDigitValue) {
                    seriesCount += 1
                } else {
                    if (maxSeriesCount < seriesCount) {
                        maxSeriesCount = seriesCount
                    }
                    seriesCount = 1
                    if (maxSeriesCount > 2) {
                        return
                    }
                }
            }
            lastChar = character
        }
        
        if (maxSeriesCount < seriesCount) {
            maxSeriesCount = seriesCount
        }
        
        delegate?.didUpdateStatus(part: .seriesOfCharacters(maxSeriesCount <= 2))
        
        return maxSeriesCount <= 2
    }
    
    private func characterCountIsCorrect(password: String) -> Bool {
        let isValid = password.count > 5
        
        delegate?.didUpdateStatus(part: .characters(isValid))
        
        return isValid
    }
    
    private func lowerCaseIsCorrect(password: String) -> Bool {
        let isValid = password.range(of: ".*[a-z].*", options: .regularExpression, range: nil, locale: nil) != nil && !password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        delegate?.didUpdateStatus(part: .lowercase(isValid))
        
        return isValid
    }
    
    private func upperCaseIsCorrect(password: String) -> Bool {
        let isValid = password.range(of: ".*[A-Z].*", options: .regularExpression, range: nil, locale: nil) != nil && !password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        delegate?.didUpdateStatus(part: .uppercase(isValid))
        
        return isValid
    }
    
    private func numberIsCorrect(password: String) -> Bool {
        let isValid = password.range(of: ".*\\d.*", options: .regularExpression, range: nil, locale: nil) != nil && !password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
        
        delegate?.didUpdateStatus(part: .number(isValid))
        
        return isValid
    }
    
    private func specialCharactersAndSpacesIsCorrect(password: String) -> Bool {
        let isValid = password.range(of: ".*[^A-Za-z0-9].*", options: .regularExpression, range: nil, locale: nil) == nil &&
            password.trimmingCharacters(in: .whitespacesAndNewlines).count == password.count && !password.isEmpty
        
        delegate?.didUpdateStatus(part: .specialCharacters(isValid))
        
        return isValid
    }
    
    private func englishCharactersIsCorrect(password: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        let isValid = password.rangeOfCharacter(from: characterset.inverted) == nil && !password.isEmpty
        
        delegate?.didUpdateStatus(part: .englishCharacters(isValid))
        
        return isValid
    }
}
