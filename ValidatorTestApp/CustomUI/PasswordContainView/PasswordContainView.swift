//
//  PasswordContainView.swift
//  ValidatorTestApp
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import UIKit

class PasswordContainView: UIView {
    
    enum ContainPart {
        case englishCharacters(Bool)
        case characters(Bool)
        case lowercase(Bool)
        case uppercase(Bool)
        case number(Bool)
        case specialCharacters(Bool)
        case seriesOfCharacters(Bool)
    }
    
    @IBOutlet var containerView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var minimumCharactersLabel: UILabel!
    @IBOutlet private weak var minimumCharactersImageView: UIImageView!
    @IBOutlet private weak var onlyEnglishCharactersLabel: UILabel!
    @IBOutlet private weak var onlyEnglishCharactersImageView: UIImageView!
    @IBOutlet private weak var oneLowercaseImageView: UIImageView!
    @IBOutlet private weak var oneLowercaseLabel: UILabel!
    @IBOutlet private weak var oneUppercaseImageView: UIImageView!
    @IBOutlet private weak var oneUppercaseLabel: UILabel!
    @IBOutlet private weak var oneNumberImageView: UIImageView!
    @IBOutlet private weak var oneOneNumberLabel: UILabel!
    @IBOutlet private weak var specialCharactersAndSpacesLabel: UILabel!
    @IBOutlet private weak var specialCharactersAndSpacesImageView: UIImageView!
    @IBOutlet private weak var seriesOfCharactersImageView: UIImageView!
    @IBOutlet private weak var seriesOfCharactersLabel: UILabel!
    
    var viewModel = PasswordContainViewViewModel()
    let nibName = "PasswordContainView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        xibSetUp()
        setupViewModel()
    }
    
    func xibSetUp() {
        containerView = loadViewFromNib()
        containerView.frame = self.bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(containerView)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
        
    }
    
    private func setupViewModel() {
        viewModel.delegate = self
    }
    
    private func toggleImageVisible(part: ContainPart) {
        let viewAndVisability = getViewAndVisability(part: part)
        let imageView = viewAndVisability.0
        let isVisible = viewAndVisability.1
        
        imageView.image = isVisible ? UIImage(named: "correct") : UIImage(named: "incorrect")
    }
    
    private func getViewAndVisability(part: ContainPart) -> (UIImageView, Bool) {
        switch part {
        case .characters(let visible):
            return (minimumCharactersImageView, visible)
        case .lowercase(let visible):
            return (oneLowercaseImageView, visible)
        case .uppercase(let visible):
            return (oneUppercaseImageView, visible)
        case .number(let visible):
            return (oneNumberImageView, visible)
        case .englishCharacters(let visible):
            return (onlyEnglishCharactersImageView, visible)
        case .specialCharacters(let visible):
            return (specialCharactersAndSpacesImageView, visible)
        case .seriesOfCharacters(let visible):
            return (seriesOfCharactersImageView, visible)
        }
    }
}

extension PasswordContainView: PasswordContainViewViewModelProtocol {
    func didUpdateStatus(part: ContainPart) {
        self.toggleImageVisible(part: part)
    }
}
