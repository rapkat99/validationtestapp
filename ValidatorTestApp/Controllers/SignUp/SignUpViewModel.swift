//
//  SignUpViewModel.swift
//  ValidatorTestApp
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import Foundation

protocol SignUpViewModelProtocol {
    func showEmailValidationError()
    func showEmailValidationSuccess()
    func showPasswordValidationError()
    func showPasswordValidationSuccess()
}

class SignUpViewModel {
    var delegate: SignUpViewModelProtocol?
    
    var isEmailCorrect = false
    var isPasswordCorrect = false
    
    private(set) var email: String = ""
    private(set) var password: String = ""
    
    
    func update(email: String?) {
        self.email = email?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    
    func update(password: String?) {
        self.password = password ?? ""
    }
    
    func checkPasswordValidation() {
        if !isPasswordCorrect {
            delegate?.showPasswordValidationError()
        } else {
            delegate?.showPasswordValidationSuccess()
        }
    }
    
    func checkEmailValidation() {
        if email.isEmpty {
            isEmailCorrect = false
            delegate?.showEmailValidationError()
        } else if isValidEmail(email: email) {
            isEmailCorrect = true
            delegate?.showEmailValidationSuccess()
        } else {
            isEmailCorrect = false
            delegate?.showEmailValidationError()
        }
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
