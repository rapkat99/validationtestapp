//
//  SignUpViewController.swift
//  ValidatorTestApp
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import UIKit

class SignUpViewController: UIViewController {
    @IBOutlet private weak var passwordContainView: PasswordContainView!
    @IBOutlet private weak var emailTextField: UITextField! {
        didSet {
            emailTextField.addTarget(self, action: #selector(emailFieldDidChange(_:)), for: .editingChanged)
            emailTextField.keyboardType = .emailAddress
            emailTextField.delegate = self
        }
    }
    @IBOutlet private weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.addTarget(self, action: #selector(passwordFieldDidChange(_:)), for: .editingChanged)
            passwordTextField.delegate = self
        }
    }
    
    var viewModel = SignUpViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        passwordContainView.isHidden = true
        hideKeyboardWhenTappedAround()
        viewModel.delegate = self
    }
    
    private func checkPassword() {
        let isPasswordCorrect = passwordContainView.viewModel.checkIfPasswordIsCorrect(password: viewModel.password)
        passwordContainView.isHidden = isPasswordCorrect
        viewModel.isPasswordCorrect = isPasswordCorrect
    }
    
    private func passwordContainViewState() {
        passwordContainView.isHidden = true
    }
    
    @objc func passwordFieldDidChange(_ textField: UITextField) {
        viewModel.update(password: textField.text)
        checkPassword()
    }
    
    @objc func emailFieldDidChange(_ textField: UITextField) {
        viewModel.update(email: textField.text)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == passwordTextField {
            checkPassword()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case emailTextField:
            viewModel.checkEmailValidation()
        case passwordTextField:
            passwordContainViewState()
            viewModel.checkPasswordValidation()
        default:
            return
        }
    }
}

extension SignUpViewController: SignUpViewModelProtocol {
    func showEmailValidationError() {
        self.emailTextField.layer.borderWidth = 2
        self.emailTextField.layer.cornerRadius = 5
        self.emailTextField.layer.borderColor = UIColor.red.cgColor
    }
    
    func showEmailValidationSuccess() {
        self.emailTextField.layer.borderWidth = 2
        self.emailTextField.layer.cornerRadius = 5
        self.emailTextField.layer.borderColor = UIColor.green.cgColor
    }
    
    func showPasswordValidationError() {
        self.passwordTextField.layer.borderWidth = 2
        self.passwordTextField.layer.cornerRadius = 5
        self.passwordTextField.layer.borderColor = UIColor.red.cgColor
    }
    
    func showPasswordValidationSuccess() {
        self.passwordTextField.layer.borderWidth = 2
        self.passwordTextField.layer.cornerRadius = 5
        self.passwordTextField.layer.borderColor = UIColor.green.cgColor
    }
}
