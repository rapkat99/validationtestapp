//
//  UIViewController+Extension.swift
//  ValidatorTestApp
//
//  Created by Baudunov Rapkat on 18/3/22.
//

import Foundation
import UIKit

fileprivate var tempView: UIView?

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
